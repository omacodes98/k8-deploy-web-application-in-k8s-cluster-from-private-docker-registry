# Demo Project - Deploy web application in k8 cluster from private Docker registry 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create Secret for credentials for the private Docker registry

* Configure the Docker registry secret in application Deployment component 

* Deploy web application image from our private Docker registry in K8s cluster

## Technologies Used 

* Kubernetes 

* Helm 

* AWS ECR 

* Docker 

## Steps 

Step 1: Get Docker login ECR password 

    aws ecr get-login-password --region eu-west-2 

[Getting ECR password](/images/01_get_docker_login_ecr_password.png)

Step 2: Ssh into minikube 

    minikube ssh 

[Ssh into minikube](/images/02_ssh_into_minikube.png)

Step 3: When in minikube docker login and paste the username, password and endpoiint of ecr 

    docker login -u AWS -p xxxx 522116691231.dkr.ecr.eu-west-2.amazonaws.com/my-app

[Docker login into minikube](/images/03_docker_login_and_past_password_in_and_endpoint_of_ecr.png)


Step 4: Check if secret docker file was created 

    ls -a 

[Secret Docker file](/images/04_secret_docker_file_created.png)

Step 5: Create secret file 

    touch docker-secret.yaml 

[Secret file created](/imaoges/05_docker_secret_file_created.png)

Step 6: Copy the docker configuration file that contains credentials from minikube to your host system with secure copy 

    scp -i $(minikube ssh-key) docker@$(minikube ip):.docker/config.json ~/.docker/config.json

[Copying configjson file](/images/06_copying_configjson_file_from_minikube_to_my_localhost.png)

Step 7: Encode config json file to base64

    cat ~/.docker/config.json | base64

[Encoding configjson to base64](/images/07_encode_config_json_file_base64.png)

Step 8: Past the encoded configjson output to secet file

[Secret config](/images/08_past_it_in_docker_secret.png)

Step 9: Apply secret 

    kubectl apply -f docker-secret.yaml

[Apply Secret](/images/09_create_secret.png)

Step 10: Create deployment file for application and reference secret in deployment config to enable docker to pull from ecr private repo 

    touch my-app-deployment.yaml

[Creating deployment file](/images/10_creating_deployment_file_for_the_application.png)
[Deployment file](/images/11_deployment_yaml_file.png)

Step 11: Apply deployment 

    kubectl apply -f my-app-deployment.yaml 

[Apply deployment](/images/12_start_app_deployment_on_K8.png)

Step 12: Check if it was successful 

    kubectl get pod 

[my app pod](/images/13_check_if_it_was_successful_image_pulled_successfully.png)


## Installation

Run $ brew install minikube 

## Usage 

Run $ kubectl apply -f my-app-deployment.yaml 


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-deploy-web-application-in-k8s-cluster-from-private-docker-registry.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-deploy-web-application-in-k8s-cluster-from-private-docker-registry

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.